import axios from 'axios';

const $axios = axios.create({
  baseURL: 'https://apiserambi.jos.id',
  // baseURL: 'http://localhost:8000',
});

export default $axios;
