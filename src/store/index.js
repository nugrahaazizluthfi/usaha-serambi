import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import register from './register/index';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    register,
  },
  state: {},
  mutations: {},
  actions: {},
  plugins: [createPersistedState()],
});

export default store;
