import Vue from 'vue';
import App from './App.vue';
// import './registerServiceWorker';
import router from './router';
import store from './store';

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

// Vue Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUserSecret,
  faChevronRight,
  faChevronLeft,
  faFilter,
  faSort,
  faCrosshairs,
  faChevronDown,
  faChevronUp,
  faSortDown,
  faSortUp,
  faCopy,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
library.add(faUserSecret);
library.add(faChevronRight);
library.add(faChevronLeft);
library.add(faFilter);
library.add(faSort);
library.add(faCrosshairs);
library.add(faChevronDown);
library.add(faChevronUp);
library.add(faSortDown);
library.add(faSortUp);
library.add(faCopy);
Vue.component('font-awesome-icon', FontAwesomeIcon);

import VueScrollTo from 'vue-scrollto';
Vue.use(VueScrollTo, {
  container: 'body',
  duration: 180,
  easing: 'ease-in',
  offset: 0,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true,
});

import Toasted from 'vue-toasted';
Vue.use(Toasted);

import vueNumeralFilterInstaller from 'vue-numeral-filter';
Vue.use(vueNumeralFilterInstaller, { locale: 'en-gb' });

import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);

import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);

import VueClipboard from 'vue-clipboard2';

VueClipboard.config.autoSetContainer = true; // add this line
Vue.use(VueClipboard);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
