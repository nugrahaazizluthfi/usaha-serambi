import Vue from 'vue';
import VueRouter from 'vue-router';
import Beranda from '../views/pages/Beranda.vue';
import DaftarAgenIndex from '../views/pages/registeragen/InputNumber.vue';
import DaftarAgenOtp from '../views/pages/registeragen/Otp.vue';
import BusinessCategory from '../views/pages/registeragen/BusinessCategory.vue';
import DaftarAgenRegister from '../views/pages/registeragen/Register.vue';
import DaftarAgenThanks from '../views/pages/registeragen/Thanks.vue';

import GenerateLinkPage from '../views/pages/GenerateLink.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/link/generator',
    name: 'generate',
    component: GenerateLinkPage,
  },
  {
    path: '/',
    name: 'Beranda',
    component: Beranda,
  },
  {
    path: '/daftar-agen-serambi',
    name: 'BusinessCategory',
    component: BusinessCategory,
  },
  {
    path: '/daftar-agen-serambi/input-number',
    name: 'inputnumber',
    component: DaftarAgenIndex,
  },
  {
    path: '/daftar-agen-serambi/otp',
    name: 'sendotp',
    component: DaftarAgenOtp,
  },
  {
    path: '/daftar-agen-serambi/register',
    name: 'register',
    component: DaftarAgenRegister,
  },
  {
    path: '/daftar-agen-serambi/thanks',
    name: 'thanks',
    component: DaftarAgenThanks,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
